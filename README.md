# getThunderbird


![version: 2.14.1](https://img.shields.io/badge/version-2.14.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)


> c'est un script bash qui télécharge les dernières versions officielles de Thunderbird, canaux possibles: 
  **latest** la release officielle et **beta**  
> les mises à jour de Thunderbird sont par défaut autorisées et gérées par Thunderbird.  
> le script se mettra éventuellement à jour, sans influence sur les canaux Thunderbird installés.  
> le script installe un canal Thunderbird pour l'utilisateur en cours.   
> le script peut désinstaller les canaux Thunderbird souhaités  
> script testé sur debian / ubuntu, mais devrait être compatible avec d'autres distributions


* les installations/désinstallations/opérations système doivent être faites avec les privilèges **root**
* la mise à jour du script ou autres opérations légères peuvent être faites en utilisateur.


## installation rapide du script

* privilèges **root** requis

```shell
wget -O getThunderbird https://framaclic.org/h/getthunderbird
chmod +x getThunderbird && ./getThunderbird
```
```text
              _  _____ _                     _           _     _         _  
    __ _  ___| ||_   _| |__  _   _ _ __   __| | ___ _ __| |__ (_)_ __ __| | 
   / _' |/ _ \ __|| | | '_ \| | | | '_ \ / _' |/ _ \ '__| '_ \| | '__/ _' | 
  | (_| |  __/ |_ | | | | | | |_| | | | | (_| |  __/ |  | |_) | | | | (_| | 
   \__, |\___|\__||_| |_| |_|\__,_|_| |_|\__,_|\___|_|  |_.__/|_|_|  \__,_| 
   |___/     version 2.10.0 - 12/06/2018

  getThunderbird 2.10.0 installé dans le système.
  maintenant, appel du script par: getThunderbird (sans ./)

```

* le script est maintenant dans le système et tout utilisateur peut s'en servir.
* **Thunderbird n'est pas encore installé**
* un canal Thunderbird peut maintenant être choisi et installé (`latest`, `beta`)


## installation d'un canal Thunderbird


```shell
getThunderbird i-canal
```

* privilèges **root** requis
* `getThunderbird p-all` possible
* la version stable en cours de Thunderbird est nommée **latest**

```text
              _  _____ _                     _           _     _         _  
    __ _  ___| ||_   _| |__  _   _ _ __   __| | ___ _ __| |__ (_)_ __ __| | 
   / _' |/ _ \ __|| | | '_ \| | | | '_ \ / _' |/ _ \ '__| '_ \| | '__/ _' | 
  | (_| |  __/ |_ | | | | | | |_| | | | | (_| |  __/ |  | |_) | | | | (_| | 
   \__, |\___|\__||_| |_| |_|\__,_|_| |_|\__,_|\___|_|  |_.__/|_|_|  \__,_| 
   |___/     version 2.10.0 - 12/06/2018

  installation Thunderbird-latest

    - téléchargement...

/tmp/getThunderbird-install_tb/thund 100%[====================================>]  47,72M  2,43MB/s    ds 17s     

   - décompression...

   - installation...

  profil Thunderbird latest configuré
  Thunderbird latest est le défaut système

  Thunderbird-latest 52.8.0 installé

```

* la dernière version officielle Thunderbird est installée, en étant directement chargée sur le site Mozilla
* un lanceur par canal installé est placé dans les menus (Applications/Internet)
* l'installation de Thunderbird sur un canal existant est refaite en **totalité**, mais **le profil 
  n'est pas modifié**
* chaque canal peut être lancé en console: `thunderbird-latest` `thunderbird-beta`
* le dernier canal installé est configuré comme défaut dans le système, c'est-à-dire comme:
   * cible de la commande `thunderbird` en console (en plus de thunderbird-canal) 
   * par défaut dans le profiles.ini de Thunderbird
* pour chaque canal, une option permet basculer cette priorité sur le canal choisi
* le canal **all** est fictif et comprend _latest+beta_
* pour chaque canal, une option permet de **copier** un éventuel profil _.default_. 
  le profil _.default_ existant est laissé en place.


## help

```shell
getThunderbird -h
```
```text
              _  _____ _                     _           _     _         _  
    __ _  ___| ||_   _| |__  _   _ _ __   __| | ___ _ __| |__ (_)_ __ __| | 
   / _' |/ _ \ __|| | | '_ \| | | | '_ \ / _' |/ _ \ '__| '_ \| | '__/ _' | 
  | (_| |  __/ |_ | | | | | | |_| | | | | (_| |  __/ |  | |_) | | | | (_| | 
   \__, |\___|\__||_| |_| |_|\__,_|_| |_|\__,_|\___|_|  |_.__/|_|_|  \__,_| 
   |___/     version 2.10.0 - 12/06/2018

      canaux possibles: latest, beta ( <all> = tous les canaux )

  exemple, installation version Release (latest): getThunderbird i-latest
  ----------------------------------------------------------------------
  getThunderbird i-canal       : installation de Thunderbird <canal> (root)
  getThunderbird d-canal       : copier un profil .default existant sur <canal>
  getThunderbird m-canal archi : installation sur le <canal> d'une <archi>ve téléchargée manuellement (root)
  getThunderbird r-canal       : désinstallation (remove) du <canal> (root)
  getThunderbird ri            : réparation icône(s) dans le menu
  getThunderbird t-canal       : téléchargement du <canal> dans le répertoire courant (sans installation)
  getThunderbird u-canal       : profil pour l'utilisateur en cours et comme défaut système (root)

  getThunderbird version       : versions installées et en ligne

    --dev   : une version de dev du script (si existante) est recherchée
    --sauve : le téléchargement est sauvegardé dans le répertoire courant en plus de l'installation
  ----------------------------------------------------------------------
  ./getThunderbird (ou ./getThunderbird -i) : installation du script dans le système (root)
  getThunderbird -h, --help    : affichage aide
  getThunderbird -r, --remove  : désinstallation du script (root)
  getThunderbird -u, --upgrade : mise à jour du script
  getThunderbird -v, --version : version du script
  ----------------------------------------------------------------------
  plus d'infos: https://framaclic.org/h/doc-getthunderbird

```


## version

```shell
getThunderbird version
```
```text
              _  _____ _                     _           _     _         _  
    __ _  ___| ||_   _| |__  _   _ _ __   __| | ___ _ __| |__ (_)_ __ __| | 
   / _' |/ _ \ __|| | | '_ \| | | | '_ \ / _' |/ _ \ '__| '_ \| | '__/ _' | 
  | (_| |  __/ |_ | | | | | | |_| | | | | (_| |  __/ |  | |_) | | | | (_| | 
   \__, |\___|\__||_| |_| |_|\__,_|_| |_|\__,_|\___|_|  |_.__/|_|_|  \__,_| 
   |___/     version 2.10.0 - 12/06/2018

  script en place: 2.10.0
  script en ligne: 2.9.1

  Thunderbird en place: latest : 52.8.0
  Thunderbird en ligne: latest : 52.8.0      beta   : 60.0b7

```


## mise à jour Thunderbird

* Thunderbird gère ses mises à jour et le script permet cet automatisme.
* cette mise à jour se fait en tâche de fond et est disponible au prochain redémarrage
* la mise à jour éventuelle peut être déclenchée manuellement avec le menu `Aide/A propos de Thunderbird`


## profil default, copie

```shell
getThunderbird d-canal
```

* `getThunderbird d-all` possible
* duplique un éventuel profil .default existant (paquet distribution par exemple) sur un canal choisi.



## nouvel utilisateur ou reconfiguration profil

```shell
getThunderbird u-canal
```

* `getThunderbird d-all` possible (plus haut canal comme défaut système)
* ajoute un profil pour un canal Thunderbird **installé**, pour l'utilisateur en cours
* configure le canal comme **défaut** dans le système
* évite de télécharger inutilement une nouvelle fois pour un nouvel utilisateur
* pour ajouter un autre utilisateur, titi par exemple: `USER_INSTALL=titi getThunderbird u-beta`, ça devrait 
  marcher (pas testé)


## désinstallation d'un canal Thunderbird


```shell
getThunderbird r-canal
```

* privilèges **root** requis
* `getThunderbird p-canal` possible
* le profil pour Thunderbird **n'est pas supprimé**, il sera donc utilisable en cas de réinstallation
* si thunderbird-canal est ouvert il sera fermé
* thunderbird sera aussi configuré pour le plus bas canal encore installé


## installation manuelle d'une archive

```shell
getThunderbird m-canal thunderbird-version.tar.bz2
```

* privilèges **root** requis
* installe une archive téléchargée manuellement   


## suppression d'un profil Thunderbird

**FERMER toutes les instances ouvertes**

**en user**, lancer en terminal le profil manager de Thunderbird: `thunderbird -P` :

* sélectionner le profil souhaité
* cliquer sur _supprimer un profil_
     * _supprimer les fichiers_, cela supprimera aussi le répertoire `~/.thunderbird/profilSélectionné`   
       le profil est **définitivement** détruit
* quitter


### suppression manuelle de tous les profils

si plus aucun canal de Thunderbird n'est installé, qu'il n'y a plus de profil manager disponible, et que vous
êtes certains de vouloir supprimer tous les profils en place, en **user**:

```shell
rm -rf ~/.thunderbird
```


## mise à jour script

```shell
getThunderbird -u
```

* test toutes les **semaines**
* mise à jour du **script** si une nouvelle version est disponible en ligne. cela n'influe pas sur les
  canaux Thunderbird installés
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée manuellement
* _anacron_ est utilisé, c'est à dire que la mise à jour sera testée, dès le redémarrage du Pc


## logs

```shell
pager /var/log/sdeb_getThunderbird.log
```

tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getThunderbird.log_   


## supprimer le script

```shell
getThunderbird -r
```

* privilèges **root** requis
* effacement du script dans le système (_/opt/bin_)
* effacement de l'inscription dans crontab/anacron utilisateur
* cela ne **supprime pas** les éventuels canaux Thunderbird installés


## sources

sur [framagit](https://framagit.org/sdeb/getThunderbird/blob/master/getThunderbird)


## changelog

sur [framagit](https://framagit.org/sdeb/getThunderbird/blob/master/CHANGELOG.md)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getThunderbird/issues)

IRC: ##sdeb@freenode.net


## license

[LPRAB/WTFPL](https://framagit.org/sdeb/getThunderbird/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/getthunderbird-gif)
